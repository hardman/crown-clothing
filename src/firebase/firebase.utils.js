// Your web app's Firebase configuration
import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyAU8v29cW1Xcm6CX6lkZmOKN9KtL98nUsc",
    authDomain: "crwn-db-c4783.firebaseapp.com",
    databaseURL: "https://crwn-db-c4783.firebaseio.com",
    projectId: "crwn-db-c4783",
    storageBucket: "crwn-db-c4783.appspot.com",
    messagingSenderId: "255678516556",
    appId: "1:255678516556:web:10f2edd9426a22b68ed3e9"
}

export const createUserProfileDocument =async (userAuth, additionalData) => {

    if(!userAuth) return;

    const userRef = firestore.doc(`users/${userAuth.uid}`);
    const snapShot = await userRef.get();

    if(!snapShot.exists) {
        const { displayName, email } = userAuth;
        const createdAt = new Date();

        try {
            await userRef.set({
                displayName,
                email,
                createdAt,
                ...additionalData
            })
        } catch (error) {
            console.log('error creating user', error.message);
        }
    }
    return userRef;
};

  firebase.initializeApp(config);

  export const auth = firebase.auth();
  export const firestore = firebase.firestore();

  const provider = new firebase.auth.GoogleAuthProvider();
  provider.setCustomParameters({ prompt: 'select_account'});
  export const signInWithGoogle = () => auth.signInWithPopup(provider);

  export default firebase;